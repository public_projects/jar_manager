import 'package:flutter/material.dart';
import 'package:jar_manager/screens/home_tabs_screen.dart';
import 'package:jar_manager/state/project_dir.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const ProjectSelectorApp());
}

class ProjectSelectorApp extends StatelessWidget {
  const ProjectSelectorApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: ProjectDirs(),
        ),
      ],
      child: MaterialApp(
        // home: ProjecstUpdateScreen(),
        home: HomeTabsScreen(),
        // home: TasksScreen()
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:jar_manager/screens/project_update_screen.dart';

class HomeTabsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: const TabBar(
            tabs: [
              Tab(icon: Icon(Icons.abc_rounded)),
              Tab(icon: Icon(Icons.directions_transit)),
              Tab(icon: Icon(Icons.directions_bike)),
            ],
          ),
          title: const Text('Main'),
        ),
        body: TabBarView(
          children: <Widget>[
            ProjecstUpdateScreen(),
            Icon(Icons.directions_transit),
            Container()
          ],
        ),
      ),
    );
  }
}

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:jar_manager/widgets/project_selection_list.dart';
import 'package:jar_manager/widgets/project_update_button.dart';

class ProjecstUpdateScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            ProjectUpdateButton(),
            Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20,),
              height: 300,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
              ),
              child: ProjectSelection(),
            ),
          )
          ],
        ),
      );
  }
}
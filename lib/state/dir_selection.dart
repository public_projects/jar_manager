import 'package:flutter/cupertino.dart';
import 'package:jar_manager/state/git_info.dart';

class DirSelection {
  List<GitInfo> gits = [];

  DirSelection(this.gits);

  factory DirSelection.fromJson(Map<String, dynamic> json) {
    List<GitInfo> details = [];
    List<dynamic> gitInfos = json['gitInfos'];

    for (var element in gitInfos) {
      String remoteUrl = element['remoteUrl'];
      String localDir = element['localDir'];

      List<dynamic> statusesResult = element['statuses'];
      List<String> statuses = [];

      for (var stat in statuses) {
        statuses.add(stat);
      }

      details.add(GitInfo(
          localDir: localDir, remoteUrl: remoteUrl, statuses: statuses));
    }
    return DirSelection(details);
  }
}

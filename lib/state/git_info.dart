class GitInfo {
  final String localDir;
  final String remoteUrl;
  final List<String> statuses;

  const GitInfo(
      {required this.localDir,
      required this.remoteUrl,
      required this.statuses});
}

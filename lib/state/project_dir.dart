import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:jar_manager/state/album.dart';
import 'package:jar_manager/state/dir_selection.dart';
import 'package:jar_manager/state/git_info.dart';

class ProjectDirs extends ChangeNotifier {
  DirSelection? dirSelection;

  void addProjectDir(dir) async {
    debugPrint('[addProjectDir] ' + dir);

    fetchAlbum();
    dirSelection = await fetchGitInfo(dir);

    notifyListeners();
  }

  DirSelection? get getDirSelection {
    return dirSelection;
  }

  Future<Album> fetchAlbum() async {
    final response = await http
        .get(Uri.parse('https://jsonplaceholder.typicode.com/albums/1'));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> temp = jsonDecode(response.body);
      debugPrint('$temp');
      return Album.fromJson(temp);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  Future<DirSelection> fetchGitInfo(String projectDirectory) async {
    final response = await http.post(
      Uri.parse('http://localhost:8180/git/v1/api/get/all'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'parentDirectory': projectDirectory,
      }),
    );

    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      // return Album.fromJson(jsonDecode(response.body));
      // debugPrint(response.body);
      Map<String, dynamic> temp = jsonDecode(response.body);
      return DirSelection.fromJson(temp);
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to get git info.');
    }
  }
}

import 'package:flutter/material.dart';
import 'package:jar_manager/state/dir_selection.dart';
import 'package:jar_manager/state/project_dir.dart';
import 'package:jar_manager/widgets/project_tile.dart';
import 'package:provider/provider.dart';

class ProjectSelection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ProjectDirs projectDirsNotifier = Provider.of<ProjectDirs>(context);
    DirSelection? dirSelection = projectDirsNotifier.getDirSelection;

    return ListView.builder(
      itemCount: getGitLength(dirSelection),
      itemBuilder: (BuildContext context, int index) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              nullCheck(dirSelection, index),
              style: TextStyle(color: Colors.black, fontSize: 23),
            ),
            Row(
              children: [
                DropdownButton<String>(
                  icon: const Icon(Icons.arrow_downward),
                  elevation: 16,
                  style: const TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String? newValue) {},
                  items: <String>['One', 'Two', 'Free', 'Four']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
                IconButton(
                  alignment: Alignment.center,
                  onPressed: () {
                    print('remove');
                  },
                  icon: Icon(Icons.delete_forever),
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  int getGitLength(DirSelection? value) {
    if (value == null) return 0;
    return value.gits.length;
  }

  String nullCheck(DirSelection? value, int index) {
    if (value == null) return index.toString();
    print('[nullCheck]'+ value.gits[index]);
    return value.gits[index].localDir;
  }
}

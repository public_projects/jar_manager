import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:jar_manager/state/project_dir.dart';
import 'package:provider/provider.dart';

class ProjectUpdateButton extends StatelessWidget {
  void _addProject(BuildContext context, String selectedDirectory) {
    print('_addProject');
    print(selectedDirectory);
    Provider.of<ProjectDirs>(context, listen: false)
        .addProjectDir(selectedDirectory);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
      padding: EdgeInsets.all(5),
      child: ElevatedButton(
        child: Text('Add Project to Update'),
        onPressed: () async {
          String? selectedDirectory =
              await FilePicker.platform.getDirectoryPath();

          if (selectedDirectory == null) {
            // User canceled the picker
          }
          print(selectedDirectory);
          _addProject(context, selectedDirectory!);
        },
      ),
    );
  }
}

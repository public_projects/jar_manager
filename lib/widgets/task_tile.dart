import 'package:flutter/material.dart';

class TaskTile extends StatefulWidget {
  @override
  State<TaskTile> createState() => _TaskTileState();
}

class _TaskTileState extends State<TaskTile> {
  bool isChecked = false;

  void checkboxCallback(bool isChecked) {
    setState(() {
      isChecked = isChecked;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        'This is a task',
        style: TextStyle(
            decoration: isChecked ? TextDecoration.lineThrough : null),
      ),
      trailing: TaskCheckBox(
          checkboxState: isChecked,
          toggleCheckboxState: checkboxCallback
          ),
    );
  }
}

class TaskCheckBox extends StatelessWidget {
  // in statelessWidget all the properties has to be final
  final bool checkboxState;
  final Function toggleCheckboxState;

  TaskCheckBox({required this.checkboxState, required this.toggleCheckboxState});

  @override
  Widget build(BuildContext context) {
    return Checkbox(
      activeColor: Colors.lightBlue,
      value: checkboxState,
      // onChanged: toggleCheckboxState,
      onChanged: null,
    );
  }
}
